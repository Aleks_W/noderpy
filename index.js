import express from 'express';
import multer from 'multer';


//import * as lib from './include/lib.js';

const app = express();
const upload = multer({ dest: 'uploads/' });



//app.use("/static", express.static(__dirname + "/public"));
app.use(express.static(__dirname));


app.post("/", upload.fields([{ name: 'downloaded_file'/*, maxCount: 4*/ }]), (req, res) => {

    console.log(req.files);
    res.send(req.files);

});

app.get("/", (req, res) => {

    res.sendFile('123.html' , { root : __dirname});

});



app.listen(3000);
/**
 * простая синхронная функция таймаута
 *
 * Пример обратного вызова:
 * wait(5000,"doSomething();");
 *
 * пример без обратного вызова:
 * console.log("Instant!");
 * wait(5000); 
 * console.log("5 second delay");
 *
 * @param ms
 * @param cb
 */
export function wait(ms, cb) {
    var waitDateOne = new Date();
    while ((new Date()) - waitDateOne <= ms) {
        //Nothing
    }
    if (cb) {
        eval(cb);
    }
}
